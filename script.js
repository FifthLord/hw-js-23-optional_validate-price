
let priseInput = document.querySelector('.price-input');
priseInput.addEventListener('focusin', addStyle);
priseInput.addEventListener('focusout', addSpan);

let form = document.querySelector('.number-form');
let showPriceWrap = document.querySelector('.show-price-wrap');
let errSpan;

function addSpan(e) {
   if (errSpan) errSpan.remove();
   if (e.target.value >= 0 && e.target.value) {
      showPriceWrap.insertAdjacentHTML('afterBegin', `
      <span class='price-span'>Поточна ціна: ${e.target.value} $</span>
      <button class='skip-prise-btn'>X</button>`)
      skipPriseBtn = document.querySelector('.skip-prise-btn')
      skipPriseBtn.addEventListener('click', deleteSpan)

      showPriceWrap.style.border = '1px solid grey'
      priseInput.style.border = '1px solid black'
      priseInput.style.color = 'green'
      priseInput.setAttribute('disabled', '')
   } else {
      priseInput.style.border = '1px solid red'
      priseInput.style.color = 'red'
      form.insertAdjacentHTML('afterEnd', `
      <span style = "color: red" class='err-span'>Please enter correct price</span>`)
      errSpan = document.querySelector('.err-span')
   }
}

function addStyle() {
   priseInput.value = '';
   priseInput.style.border = '1px solid green';
   priseInput.style.color = '';
}

function deleteSpan() {
   showPriceWrap.innerHTML = '';
   showPriceWrap.style.cssText = '';
   priseInput.value = '';
   priseInput.style.color = '';
   priseInput.removeAttribute('disabled');
}